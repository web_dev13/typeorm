import "reflect-metadata"
import { DataSource } from "typeorm"
import { Products } from "./entity/Products"
import { User } from "./entity/User"

export const AppDataSource = new DataSource({
    type: "sqlite",
    database: "database.sqlite",
    synchronize: true,
    logging: false,
    entities: [User,Products],
    migrations: [],
    subscribers: [],
})
