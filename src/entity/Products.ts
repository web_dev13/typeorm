import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Products{
    @PrimaryGeneratedColumn({name: "product_id"})
    id:number;

    @Column({name: "product_name"})
    name: string;

    @Column({name: "product_price"})
    price: number;

}