import { AppDataSource } from "./data-source"
import { Products } from "./entity/Products"

AppDataSource.initialize().then(async () => {

    console.log("Inserting a new user into the database...")
    const productRepository = AppDataSource.getRepository(Products)
    // const product = new Products()
    // product.name = "Kao pad kai"
    // product.price = 50
    // await productRepository.save(product)
    // console.log("Saved a new product with id: " + product.id)

    // console.log("Loading products from the database...")
    const products = await productRepository.find()
    console.log("Loaded products: ", products)

    // console.log("Here you can setup and run express / fastify / any other framework.")

    const updateProduct = await productRepository.findOneBy({id:1})
    console.log(updateProduct);
    updateProduct.price=80
    await productRepository.save(updateProduct)
    
}).catch(error => console.log(error))
